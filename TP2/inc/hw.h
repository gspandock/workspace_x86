#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc

#define FICHA  49  // ASCII para la tecla F
#define CAFE  50  // ASCII para la tecla C
#define TE  51  // ASCII para la tecla T


/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_Led(void);

void hw_IngresoFicha(void);

void hw_ActivarElectrovalavulaTe(void);
void hw_ApagarElectrovalavulaTe(void);

void hw_ActivarElectrovalavulaCafe(void);
void hw_ApagarElectrovalavulaCafe(void);



void hw_ActivarAlarma(void);
void hw_ApagarAlarma(void);

void hw_DevolverFicha(void);




/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
