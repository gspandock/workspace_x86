#ifndef _EXPENDEDORA_H_ // ¿QUE ES?
#define _EXPENDEDORA_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

//DEFINO LOS ESTADOS 
typedef enum {
    REPOSO,  
    INGRESANDO_BEBIDA, // INGRESANDO
    SERVIR_TE, //ESPERANDO_EGRESO
    SERVIR_CAFE, // NO HABIA
    ALARMA              // ALARMA 
} FSM_EXPENDEDORA_STATES_T; //¿que hace?

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void fsm_expendedora_init(void); //ESTADO INICIAL
void fsm_expendedora_runCycle(void);  //LOOP DE ESTADOS

// Eventos
void fsm_expendedora_raise_evFichaIngresada_On(void);
void fsm_expendedora_raise_evIngresoTe_On(void);
void fsm_expendedora_raise_evIngresoCafe_On(void);
void fsm_expendedora_raise_evTick1seg(void);
void fsm_expendedora_raise_evTick02seg(void);

// Debugging
void fsm_expendedora_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _EXPENDEDORA_H_ */
