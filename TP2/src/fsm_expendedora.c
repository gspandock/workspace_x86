/*==================[inclusions]=============================================*/

#include "fsm_expendedora.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_ELEGIR  30
#define ESPERA_SERVIR_TE  30 
#define ESPERA_SERVIR_CAFE 45
#define ESPERA_ALARMA 2

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_EXPENDEDORA_STATES_T state;

bool evFichaIngresada_On_raised;//evSensor1_On
bool evIngresoTe_On_raised;// evSensor2_On
bool evIngresoCafe_On_raised;// evSensor2_Off 
bool evTick1seg_raised;//evTtick1seg
bool evTick02seg_raised;
uint8_t count_seg;


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

//TODOS LOS EVENTOS EN CERO ("TODOS LOS SENSORES SIN LECTURA")

static void clearEvents(void)
{
    evFichaIngresada_On_raised = 0;
    evIngresoTe_On_raised = 0;
    evIngresoCafe_On_raised = 0;
    evTick1seg_raised = 0;
    evTick02seg_raised = 0;

}

/*==================[external functions definition]==========================*/

void fsm_expendedora_init(void)
{
    state = REPOSO;
    clearEvents();
}

void fsm_expendedora_runCycle(void)
{
    // El diagrama se encuentra en Workspace DIAGRAMA_MAQUINA_EXPENDEDORA.pdf
   
    switch (state) {
        
        case REPOSO:
            
            if (evTick1seg_raised) {
                count_seg++;
                hw_Led();
            }
          
            if (evFichaIngresada_On_raised) {
                hw_IngresoFicha();
                count_seg = 0;
                state = INGRESANDO_BEBIDA;
            }
            break;

         case INGRESANDO_BEBIDA:
            
             if (evIngresoTe_On_raised) {
                count_seg = 0;
                hw_ActivarElectrovalavulaTe();
               state = SERVIR_TE;
            }
            if (evIngresoCafe_On_raised) {
                count_seg = 0;
                hw_ActivarElectrovalavulaCafe();
                state = SERVIR_CAFE;
            }
            else if (evTick1seg_raised && (count_seg < ESPERA_ELEGIR)) {
                hw_Led(); 
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_ELEGIR)) {
                hw_DevolverFicha();
                count_seg = 0;
                state = REPOSO;
            }
            break;

            
        case SERVIR_TE:
           
           
        if (evTick02seg_raised && (count_seg < (ESPERA_SERVIR_TE ))) {
                hw_Led();
            }

         if (evTick1seg_raised && (count_seg < ESPERA_SERVIR_TE)) {
                count_seg++;
            }

        else if (evTick1seg_raised && (count_seg == ESPERA_SERVIR_TE)) {
                hw_ApagarElectrovalavulaTe();
                hw_ActivarAlarma();
                count_seg = 0;
                
                state = ALARMA;
            }
            break;

         case SERVIR_CAFE:
       
           if (evTick02seg_raised && (count_seg < (ESPERA_SERVIR_CAFE ))) {
                hw_Led();
            }


           if (evTick1seg_raised && (count_seg < ESPERA_SERVIR_CAFE)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_SERVIR_CAFE)) {
                hw_ApagarElectrovalavulaCafe();
                hw_ActivarAlarma();
                count_seg = 0;
                
                state = ALARMA;
            }
            break;
        
        case ALARMA:
            
            
            if (evTick1seg_raised && (count_seg < ESPERA_ALARMA)) {
                count_seg++;
                hw_Led();  
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_ALARMA)) {
                hw_ApagarAlarma();
                count_seg = 0;
                state = REPOSO;
            }
            break;
    }

    clearEvents();
}

void fsm_expendedora_raise_evFichaIngresada_On(void)
{
    evFichaIngresada_On_raised = 1;
}

void fsm_expendedora_raise_evIngresoTe_On(void)
{
    evIngresoTe_On_raised = 1;
}

void fsm_expendedora_raise_evIngresoCafe_On(void)
{
    evIngresoCafe_On_raised = 1;
}

void fsm_expendedora_raise_evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void fsm_expendedora_raise_evTick02seg(void)
{
    evTick02seg_raised = 1;
}

void fsm_expendedora_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/
