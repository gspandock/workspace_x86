/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}



void hw_ActivarElectrovalavulaTe(void)
{
    printf("\n\n Sirviendo Té \n\n");
}


void hw_ApagarElectrovalavulaTe(void)
{
    printf("\n\n Electrovalvula de Té Cerrada \n\n");
}

void hw_ActivarElectrovalavulaCafe(void)
{
    printf("\n\n Sirviendo Café \n\n");
}

void hw_ApagarElectrovalavulaCafe(void)
{
    printf("\n\n Electrovalvula de Café Cerrada \n\n");
}


void hw_ActivarAlarma(void)
{
    printf("\n\n Alarma activada \n\n");
}

void hw_ApagarAlarma(void)
{
    printf("\n\n Alarma apagada \n\n");
}

void hw_IngresoFicha(void)
{
    printf("\n\n Ingresó Ficha \n\n");
}

void hw_DevolverFicha(void)
{
    printf("\n\n Devolver Ficha \n\n");
}

void hw_Led(void)
{
    printf("LED\t ");
}






/*==================[end of file]============================================*/
