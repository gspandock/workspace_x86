/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_expendedora.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;
    uint16_t cont_ms1 = 0;

    hw_Init();

    fsm_expendedora_init();

    while (input != EXIT) {
        input = hw_LeerEntrada();

        // En un microcontrolador estos eventos se generarian aprovechando las
        // interrupciones asociadas a los GPIO
        
        if (input == FICHA) {
            fsm_expendedora_raise_evFichaIngresada_On();
        }

        if (input == CAFE) {
            fsm_expendedora_raise_evIngresoCafe_On();
        }
        
        if(input == TE) {
            fsm_expendedora_raise_evIngresoTe_On();
        }

        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        
        cont_ms++;
        if (cont_ms == 500) {
            cont_ms = 0;
            fsm_expendedora_raise_evTick1seg();
        }

        
        cont_ms1++;
        if (cont_ms1 == 100) {
            cont_ms1 = 0;
            fsm_expendedora_raise_evTick02seg();
        }






        fsm_expendedora_runCycle();

        // Esta funcion hace que el sistema no responda a ningun evento por
        // 1 ms. Funciones bloqueantes de este estilo no suelen ser aceptables
        // en sistemas baremetal.
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
