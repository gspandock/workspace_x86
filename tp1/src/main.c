/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    int I;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    
    while (input != EXIT) {
        
    input = hw_LeerEntrada();
    
    if (input == SENSOR_1) {
            
        hw_AbrirBarrera();

    printf("\nEsperando que el auto llegue a sensor 2");    
    
    while (input != SENSOR_2){
              input = hw_LeerEntrada();   
        }   
        
        printf("\n Inicio espera de 5 segundos\n");

        for(I=0;I<50;I++) {
            //espera de 5 segundos 
            
            input = hw_LeerEntrada();
            
            if(input==SENSOR_1){
                
                printf("\n NUEVO AUTO QUIERE ENTRAR\t");
                I=0;
    
                printf("\n Esperando que el auto llegue a sensor 2\n\n");    
    
                 while (input != SENSOR_2){
                    input = hw_LeerEntrada();   
                }   
                printf("\n Inicio espera de 5 segundos\n");
                               }
            
            
        hw_Pausems(100);
        
        }

    
             
        hw_CerrarBarrera();
    } 
    
   
   if (input == SENSOR_2){
       hw_AlarmaSonando();
   }
    
    }
    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
