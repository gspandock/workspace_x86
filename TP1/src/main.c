/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;

    hw_Init();
printf("\n Lector de barrera, 1 barrera abierta, 2 barrera cerrada\n\n\n");  
    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        
        

        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
            hw_AbrirBarrera();
        }

        if (input == SENSOR_2) {
            hw_CerrarBarrera();
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
